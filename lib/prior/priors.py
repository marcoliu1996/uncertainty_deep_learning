import torch
import torch.nn as nn
if __name__ == '__main__':
	from cnn_gp_prior import CNN_GP_prior
else:
	from .cnn_gp_prior import CNN_GP_prior

class f_prior_BNN(nn.Module):
	def __init__(self, out_size, device, num_channels_output=1):
		super().__init__()
		self.P = out_size[1] * out_size[0]
		self.C = num_channels_output
		self.model = CNN_GP_prior(out_size, num_channels_output=num_channels_output)
		self.device = device
		self.diag_noise = 1e-1
	def forward(self, x_t, x_c=None):
		assert len(x_t.size()) == 4
		if x_c is not None:
			x = torch.cat((x_t, x_c), 0)
		else:
			x = x_t
		N = x.size(0)
		prior_cov = self.model.compute_K(x)
		prior_mean = torch.tensor([[-6.877115175,-1.371885583,1.822841311,0.565990165,0.545653592,-0.35459867,0.386758748]]).expand(N,7).to(self.device)
		#prior_mean = torch.cat((5*torch.ones((N, 3)).to(self.device), 0.5 * torch.ones((N, 4)).to(self.device)),dim = 1)
		for i in range(N):
			prior_cov[i,:,i] += self.diag_noise
		# print(prior_cov.shape) = torch.Size([5, 37632, 5])
		return prior_mean, prior_cov
