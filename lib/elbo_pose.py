"""
	Evidence Lower Bound (ELBO) for Functional Variational Inference (FVI)
	Depth experiments. Gaussian, Laplace and reverse Huber Likelihoods
"""

import torch
import math
from .utils.torch_utils import bmat_inv_logdet, bdiag_prod
import torch.nn.functional as F


def weight_aleatoric(c):
	dist = torch.distributions.normal.Normal(0., 1.)
	Phi_minus_c = dist.cdf(-c.sqrt())
	Z_0 = 2 * (1. - torch.exp(-c) +  torch.exp(-0.5 * c) * (2 * math.pi * c).sqrt() * Phi_minus_c)
	den = 4. - 4 * (c + 1.) * torch.exp(-c)  + 2 * c * torch.exp(-0.5 * c) * (2 * math.pi * c).sqrt() * Phi_minus_c
	result = den/Z_0
	return result

def gaussian_log_prob(samples, mean, logvar):
	aux = samples - mean
	result = - 0.5 * (torch.pow(aux, 2) * torch.exp(-logvar) + logvar + math.log(2 * math.pi))
	return result

def laplacian_log_prob(samples, mean, logvar):
	aux = samples - mean
	log_scale = 0.5 * logvar
	scale_inv = torch.exp(-log_scale)
	result = - (torch.abs(aux) * scale_inv + log_scale)
	return result

def log_lik_exact(y_t, q_mean, q_cov, logvar):
	aux = y_t - q_mean
	q_cov_trace = torch.diagonal(q_cov, offset=0, dim1=0, dim2=2).t()
	result = - 0.5 * ((torch.pow(aux, 2) + q_cov_trace) * torch.exp(-logvar) + logvar + math.log(2 * math.pi))
	return result


def pose_lik_exact(y_t,q_mean,q_cov,logvar):
	pos_aux = (y_t[:,:3] - q_mean[:,:3]) * torch.exp(-logvar[:,:3])
	ori_aux = (y_t[:,3:] - q_mean[:,3:]) * torch.exp(-logvar[:,3:])

	beta = 1500
	q_cov_trace = torch.diagonal(q_cov, offset=0, dim1=0, dim2=2).t()

	pose_loss = torch.norm(pos_aux,dim = 1) + beta * torch.norm(ori_aux,dim = 1)

	# pose_loss = torch.cat([torch.pow(pos_aux, 2), beta * torch.pow(ori_aux, 2)], dim =1)
	# result = - 0.5 * (pose_loss * torch.exp(-logvar) + logvar)
	return - (pose_loss + torch.norm(logvar,dim = 1))

def kl_div(q_mean, q_cov, p_mean, p_cov):
	N, P, _ = q_cov.size()
	jitter = 1e-3
	q_cov_stable = torch.zeros_like(q_cov)
	for i in range(N):
		q_cov_stable[i,:,i] = q_cov[i,:,i] + jitter
	_, log_det_q = bmat_inv_logdet(q_cov_stable)
	p_cov_inv, log_det_p = bmat_inv_logdet(p_cov)
	logdet = log_det_p - log_det_q
	trace = torch.diagonal(bdiag_prod(p_cov_inv, q_cov_stable), dim1=0, dim2=2).sum(1).sum(0)
	m_ = (p_mean - q_mean).unsqueeze(2)
	Q_aux = bdiag_prod(p_cov_inv, m_).contiguous().view(-1, 1)
	Q = (m_.contiguous().view(-1, 1).t() @ Q_aux).squeeze(1).squeeze(0)
	return 0.5 * (logdet - N * P + trace + Q)

def fELBO_pose(y_t, lik_logvar, q_mean, q_cov, prior_mean, prior_cov, print_loss=False):
	N = y_t.size(0)
	y_t = y_t.view(N, -1)

	P = y_t.size(1)

	log_lik_ = pose_lik_exact(y_t, q_mean[:N,:], q_cov[:N,:,:N], lik_logvar)
	log_lik = log_lik_.mean(0)
	kl = kl_div(q_mean, q_cov, prior_mean, prior_cov)
	# print(log_lik)
	# print(kl)

	if print_loss:
		logger = 'Pixel-averaged Log Likelihood: {:.3f} || Pixel-averaged KL divergence: {:.3f}'.format(log_lik.item(), kl.item())
		print(logger)

	elbo = log_lik - 1 * kl
	return elbo, log_lik, kl

