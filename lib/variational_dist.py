import torch.nn as nn
from .model.posenet.model import model_parser

class Q_PoseNet_FVI(nn.Module):
    def __init__(self, L=20, diag=True):
        super().__init__()
        self.L = L

        self.diag = diag

        self.model = model_parser(model='FVI_Res', dropout_rate=0.0, L = L)


    def forward(self, x):
        position, rotation, cov, logvar_aleatoric, _ = self.model.forward(x)

        # rotation = f.normalize(rotation, p=2, dim=1)
        # torch.Size([5, 3])
        # torch.Size([5, 4])
        # torch.Size([5, 140])

        return position.view(position.shape[0],position.shape[1],1), rotation.view(rotation.shape[0],rotation.shape[1],1), cov.view(cov.shape[0],7,self.L), logvar_aleatoric.view(logvar_aleatoric.shape[0],7,1)
#
# class Q_FCDenseNet103_FVI(nn.Module):
#     def __init__(self, in_channels=3, down_blocks=(4,5,7,10,12),
#                  up_blocks=(12,10,7,5,4), bottleneck_layers=15,
#                  growth_rate=16, out_chans_first_conv=48, L=20,
#                  out_chans=None, p=0., diag=True):
#         super().__init__()
#         self.L = L
#         self.out_chans = out_chans
#         self.diag = diag
#         if out_chans is None:
#             if diag:
#                 out_chans_last = 1 + L + 1 + 1
#             else:
#                 out_chans_last = 1 + L + 1
#         else:
#             if diag:
#                 out_chans_last = out_chans + L * out_chans + out_chans + out_chans
#             else:
#                 out_chans_last = out_chans + L * out_chans + out_chans
#         self.model = FCDenseNet_103(in_channels, down_blocks, up_blocks,
#                                     bottleneck_layers, growth_rate, out_chans_first_conv,
#                                     out_chans_last=out_chans_last, p=p)
#
#     def forward(self, x):
#         out = self.model.forward(x)
#         # out.shape: torch.Size([5, 23, 168, 224])
#         # x.shape: torch.Size([5, 3, 168, 224])
#         if self.out_chans is None:
#             C = 1
#         else:
#             C = self.out_chans
#         out_mean = out[:,:C,:,:]
#         out_cov = out[:,C:(C + C*self.L),:,:]
#         # torch.Size([5, 1, 168, 224]) # out[0]
#         # torch.Size([5, 20, 168, 224]) # out[1:20]
#
#         if self.diag:
#                 out_cov_diag = out[:,(C + C*self.L):(C + C + C*self.L),:,:]
#                 out_logvar_aleatoric = out[:,(C + C + C*self.L):(C + C + C + C*self.L),:,:]
#                 return out_mean, out_cov, out_cov_diag, out_logvar_aleatoric
#         else:
#                 out_logvar_aleatoric = out[:,(C + C*self.L):,:,:]       #out[21:22]
#                 return out_mean, out_cov, out_logvar_aleatoric
#
# class Q_FCDenseNet103_MCDropout(nn.Module):
#     def __init__(self, in_channels=3, down_blocks=(4,5,7,10,12),
#                  up_blocks=(12,10,7,5,4), bottleneck_layers=15,
#                  growth_rate=16, out_chans_first_conv=48, out_chans=1, p=0.2, use_bn=True):
#
#         super().__init__()
#         self.out_chans = out_chans
#         self.model = FCDenseNet_103(in_channels, down_blocks, up_blocks,
#                                     bottleneck_layers, growth_rate, out_chans_first_conv,
#                                     out_chans_last=2*out_chans, p=p, use_bn=use_bn)
#
#     def forward(self, x):
#         out = self.model(x)
#         out_mean = out[:,:self.out_chans,:,:]
#         out_logvar_aleatoric = out[:,self.out_chans:,:,:]
#         return out_mean, out_logvar_aleatoric
#
