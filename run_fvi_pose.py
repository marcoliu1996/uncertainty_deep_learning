import torch
import numpy as np
import argparse
import os

from lib.fvi_pose import FVI
from lib.utils.fvi_depth_utils import run_test_fvi_per_image, run_runtime_fvi
import torch.nn.functional as F
from lib.model.posenet.pose_utils import position_dist, rotation_dist
parser = argparse.ArgumentParser()

parser.add_argument('--batch_size', type=int, default=4)
parser.add_argument('--lr', type=float, default=1e-4)
parser.add_argument('--randseed', type=int, default=0)
parser.add_argument('--n_epochs', type=int, default=120)
parser.add_argument('--dataset', type=str, default='cambridge', help='cambridge, interiornet or make3d')
parser.add_argument('--add_cov_diag', type=bool, default=True, help='Add Diagonal component to Q covariance')
parser.add_argument('--f_prior', type=str, default='cnn_gp', help='Type of GP prior: cnn_gp')
parser.add_argument('--x_inducing_var', type=float, default=0.1, help='Pixel-wise variance for inducing inputs')
parser.add_argument('--n_inducing', type=int, default=1, help='No. of inducing inputs, <= batch_size')
parser.add_argument('--save_results', type=int, default=100, help='save results every few epochs')
parser.add_argument('--base_dir', type=str, default='./', help='directory in which datasets are contained')
parser.add_argument('--training_mode', action='store_true')
parser.add_argument('--load', action='store_true', help='Load model for resuming training, default: False')                                                                 
parser.add_argument('--test_mode', action='store_true')                                                                                                                     
parser.add_argument('--test_runtime_mode', action='store_true')

def load_ood(args):
	path = os.path.join(args.base_dir, 'dataset/KingsCollege')
	meta_path = os.path.join(args.base_dir, 'dataset/KingsCollege/dataset_train.txt')
	data_loader = get_loader('Resnet', path, meta_path, 'train', 1,
							 is_shuffle = True)
	ood_set = data_loader['train']
	return ood_set

args = parser.parse_args()

if args.training_mode:
	torch.backends.cudnn.benchmark = True #when the input dimensions do not vary much, accelerates run time
elif args.test_runtime_mode:
	torch.backends.cudnn.deterministic = True
np.random.seed(args.randseed)
torch.manual_seed(args.randseed)
torch.cuda.manual_seed(args.randseed)

params = {'batch_size': args.batch_size,
          'shuffle': True,
          'num_workers': 0}

from lib.model.posenet.data_loader import get_loader
H, W = 224, 224
image_path = os.path.join(args.base_dir, 'dataset/ShopFacade')
metadata_path = os.path.join(args.base_dir, 'dataset/ShopFacade/dataset_train.txt')

data_loader = get_loader('Resnet', image_path, metadata_path, 'train', args.batch_size,
						 is_shuffle = True)
print('Make3d train data dir: ', metadata_path)
training_set = data_loader['train']
test_set = data_loader['val']
ood_set = load_ood(args)
exp_name = '{}_fvi_pose_gp_bnn'.format(args.dataset)

from lib.elbo_pose import fELBO_pose as fELBO


def lik_loss(y_t,q_mean,logvar):
	pos_aux = (y_t[:,:3] - q_mean[:,:3]) * torch.exp(-logvar[:,:3])
	ori_aux = (y_t[:,3:] - q_mean[:,3:]) * torch.exp(-logvar[:,3:])
	beta = 1500
	# q_cov_trace = torch.diagonal(q_cov, offset=0, dim1=0, dim2=2).t()

	pose_loss = torch.norm(pos_aux,dim = 1) + beta * torch.norm(ori_aux,dim = 1)

	# pose_loss = torch.cat([torch.pow(pos_aux, 2), beta * torch.pow(ori_aux, 2)], dim =1)
	# result = - 0.5 * (pose_loss * torch.exp(-logvar) + logvar)
	return - (pose_loss + torch.norm(logvar,dim = 1))

def angle_between_quat(quat_a,quat_b):
	from pyquaternion import Quaternion
	q0 = Quaternion(quat_a)
	q1 = Quaternion(quat_b)
	q1_inv = q1.inverse
	q = q0*q1_inv
	q_img_norm = (q[1]**2 + q[2]**2 + q[3]**2)**0.5
	diff = 2*np.arcsin(q_img_norm)*180/3.1416
	return diff if diff<180 else abs(180-diff)


N_test = test_set.__len__()
def train(num_epochs, FVI):
	import csv
	with open('fvi_loss_log_shop.csv','w', newline='') as loss_file:
		writer = csv.writer(loss_file, delimiter=',')
		FVI.train()
		# N_train = training_set.__len__()
		N_train = 231
		for s in range(args.n_epochs + 1):
			train_ll = 0.
			lik_ll = 0.
			kl_ll = 0.
			FVI.train()
			for i, (inputs, poses) in enumerate(training_set):
				x_t = inputs.to(device)
				y_t = poses.to(device)
				x_ood,_ = next(iter(ood_set))
				x_ood = x_ood.to(device)
				lik_logvar, q_mean, q_cov, prior_mean, prior_cov = FVI(x_t,x_ood)
				# print(torch.norm(q_cov))
				# print(torch.norm(prior_cov))
				# print(torch.norm(q_mean-prior_mean))
				# print('------------------')
				loss_minus,lik,kl = fELBO(y_t, lik_logvar, q_mean,
						q_cov, prior_mean, prior_cov, print_loss=False)

				optimizer.zero_grad()
				loss = - loss_minus
				loss.backward()
				train_ll += loss.item() * (x_t.size(0))
				lik_ll += lik.item() * (x_t.size(0))
				kl_ll += kl.item() * (x_t.size(0))
				torch.nn.utils.clip_grad_norm_(FVI.q.parameters(), 1.)
				optimizer.step()

			FVI.eval()
			val_loss = .0
			pos_err = .0
			ori_err = .0
			log_sigma = .0
			epistemic_cov = .0
			for i, (inputs, poses) in enumerate(test_set):
				x_t = inputs.to(device)
				y_t = poses.to(device)
				log_var, q_mean, q_cov, _,_ = FVI(x_t,x_ood)
				log_sigma += torch.norm(log_var)
				epistemic_cov += q_cov[0,:,0].mean(0)
				print(q_cov[0,:,0])
				print(log_var)
				N = log_var.size(0)
				val_loss += lik_loss(y_t,q_mean[:1,:],log_var[:1,:])
				ori_true = y_t[:,3:]
				ori_out = q_mean[:1,3:]
				pos_true = y_t[:,:3]
				pos_out = q_mean[:1,:3]

				pos_err += position_dist(pos_out[0].detach().cpu().numpy(),pos_true[0].cpu().numpy())
				ori_err += abs(angle_between_quat(ori_out[0].detach().cpu().numpy(),ori_true[0].cpu().numpy()))
			val_loss /= N_test
			pos_err /= N_test
			ori_err /= N_test
			log_sigma /= N_test
			epistemic_cov /= N_test

			train_ll /= N_train
			lik_ll /= N_train
			kl_ll /= N_train
			writer.writerow([train_ll,lik_ll,-kl_ll,val_loss.item(),pos_err,ori_err,log_sigma.item(),epistemic_cov.item()])
			# print(lik_ll)
			print('Epoch {}: training loss = {}'.format(s,[train_ll, lik_ll, -kl_ll, val_loss.item(),pos_err,ori_err, log_sigma.item(),epistemic_cov.item()]))
if __name__ == '__main__':
	device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
	keys = ('device', 'x_inducing_var', 'f_prior', 'n_inducing', 'add_cov_diag')
	values = (device, args.x_inducing_var, args.f_prior, args.n_inducing, args.add_cov_diag)
	fvi_args = dict(zip(keys, values))
	FVI = FVI(x_size=(H, W), **fvi_args).to(device)
	optimizer = torch.optim.AdamW(FVI.parameters(), lr=args.lr, weight_decay=1e-4)
	torch.set_printoptions(threshold=999999)

	if args.load:
		model_load_dir = os.path.join(args.base_dir, 'FVI_CV/model_{}_{}.bin'.format(args.dataset, exp_name))
		optimizer_load_dir = os.path.join(args.base_dir, 'FVI_CV/optimizer_{}_{}.bin'.format(args.dataset, exp_name)) 
		FVI.load_state_dict(torch.load(model_load_dir))
		optimizer.load_state_dict(torch.load(optimizer_load_dir))
		print('Loading FVI gaussian model..')

	if args.training_mode:
		print('Training FVI gaussian for {} epochs'.format(args.n_epochs))
		train(args.n_epochs, FVI)
	if args.test_mode:
		print('FVI gaussian on test mode')
		load_dir_model = os.path.join(args.base_dir, 'FVI_CV/models_test/model_{}_fvi_gaussian_test.bin'.format(args.dataset))
		FVI.load_state_dict(torch.load(load_dir_model))
		run_test_fvi_per_image(-1, FVI, test_set, N_test, args.dataset, exp_name, 'gaussian', mkdir=True)
	if args.test_runtime_mode:
		load_dir_model = os.path.join(args.base_dir, 'FVI_CV/models_test/model_{}_fvi_gaussian_test.bin'.format(args.dataset))
		FVI.load_state_dict(torch.load(load_dir_model))
		run_runtime_fvi(FVI, test_set, 'gaussian', exp_name)
