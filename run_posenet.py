import torch
import numpy as np
import argparse
import os

import torch.nn.functional as F
from lib.model.posenet.pose_utils import position_dist, rotation_dist
from lib.model.posenet.power_spherical_distribution import HypersphericalUniform, MarginalTDistribution, PowerSpherical

parser = argparse.ArgumentParser()

parser.add_argument('--batch_size', type=int, default=16)
parser.add_argument('--lr', type=float, default=1e-4)
parser.add_argument('--randseed', type=int, default=0)
parser.add_argument('--n_epochs', type=int, default=115)
parser.add_argument('--save_results', type=int, default=100, help='save results every few epochs')
parser.add_argument('--base_dir', type=str, default='./', help='directory in which datasets are contained')
parser.add_argument('--training_mode', action='store_true')


args = parser.parse_args()

if args.training_mode:
    torch.backends.cudnn.benchmark = True #when the input dimensions do not vary much, accelerates run time
elif args.test_runtime_mode:
    torch.backends.cudnn.deterministic = True
np.random.seed(args.randseed)
torch.manual_seed(args.randseed)
torch.cuda.manual_seed(args.randseed)

params = {'batch_size': args.batch_size,
          'shuffle': True,
          'num_workers': 0}

from lib.model.posenet.data_loader import get_loader
H, W = 224, 224
image_path = os.path.join(args.base_dir, 'dataset/ShopFacade')
metadata_path = os.path.join(args.base_dir, 'dataset/ShopFacade/dataset_train.txt')

data_loader = get_loader('Resnet', image_path, metadata_path, 'train', args.batch_size,
                         is_shuffle = True)
print('Make3d train data dir: ', metadata_path)
training_set = data_loader['train']
test_set = data_loader['val']


def lik_loss(y_t,pos,ori):

    pos_aux = y_t[:, :3] - pos
    ori_aux = y_t[:, 3:] - ori

    beta = 1500
    # print(pos_aux)
    # print(torch.norm(pos_aux,dim = 1))
    pose_loss = torch.norm(pos_aux,dim = 1).mean(0) + beta * torch.norm(ori_aux,dim = 1).mean(0)
    return pose_loss
    # pose_loss = torch.cat([torch.pow(pos_aux,2), beta * torch.pow(ori_aux,2)], dim = 1)
    # print(torch.norm(pose_loss, dim = 1))
    # return torch.norm(pose_loss, dim = 1)

# def ps_lik_loss(y_t,pos,ori):

#     pos_aux = y_t[:, :3] - pos
#     ps_dist = PowerSpherical(y_t[:,3:], 200.)
#     ori_lik = torch.exp(ps_dist.log_prob(ori))
    
#     beta = 10
#     # print(torch.norm(pos_aux,dim = 1).mean(0))
#     # print(ori_lik.mean(0))
#     # pose_loss = torch.norm(pos_aux,dim = 1).mean(0) - beta * ori_lik.mean(0)
#     pose_loss = torch.sum(torch.pow(pos_aux,2),dim = 1).mean(0) - beta * ori_lik.mean(0)

#     return pose_loss
#     # pose_loss = torch.cat([torch.pow(pos_aux,2), beta * torch.pow(ori_aux,2)], dim = 1)
#     # print(torch.norm(pose_loss, dim = 1))
#     # return torch.norm(pose_loss, dim = 1)
def ps_lik_loss(y_t,pos,ori):

    pos_aux = y_t[:, :3] - pos
    # ps_dist = PowerSpherical(y_t[:,3:], 200.)
    # ori_lik = torch.exp(ps_dist.log_prob(ori))
    N = ori.size(0)
    ori_lik = (torch.ones((N,)).to(device) + torch.bmm(y_t[:,3:].view(N,1,4),ori.view(N,4,1)).view(N,))
    beta = 10
    # print(ori_lik.mean(0))
    # print(torch.sum(torch.pow(pos_aux,2),dim = 1).mean(0))
    # pose_loss = torch.norm(pos_aux,dim = 1).mean(0) - beta * ori_lik.mean(0)
    pose_loss = torch.sum(torch.pow(pos_aux,2),dim = 1).mean(0) - beta * ori_lik.mean(0)

    return pose_loss, torch.sum(torch.pow(pos_aux,2),dim = 1).mean(0).item(), - beta * ori_lik.mean(0).item()
    # pose_loss = torch.cat([torch.pow(pos_aux,2), beta * torch.pow(ori_aux,2)], dim = 1)
    # print(torch.norm(pose_loss, dim = 1))
    # return torch.norm(pose_loss, dim = 1)

def val_lik_loss(y_t,pos,ori):
    pos_aux = y_t[:, :3] - pos
    ori_aux = y_t[:, 3:] - ori
    beta = 100

    pose_loss = torch.norm(pos_aux) + beta * torch.norm(ori_aux)
    return pose_loss

def angle_between_quat(quat_a,quat_b):
    from pyquaternion import Quaternion
    q0 = Quaternion(quat_a)
    q1 = Quaternion(quat_b)
    q1_inv = q1.inverse
    q = q0*q1_inv
    q_img_norm = (q[1]**2 + q[2]**2 + q[3]**2)**0.5
    diff = 2*np.arcsin(q_img_norm)*180/3.1416
    return diff if diff<180 else abs(180-diff)


N_test = test_set.__len__()
from lib.model.posenet.model import model_parser
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model = model_parser(model='Resnet', dropout_rate=0.0).to(device)
optimizer = torch.optim.AdamW(model.parameters(), lr=args.lr, weight_decay=1e-4)
torch.set_printoptions(threshold=999999)

import csv
with open('pose_net_sphere_shop.csv','w', newline='') as loss_file:
    writer = csv.writer(loss_file, delimiter=',')
    N_train = training_set.__len__()
    for s in range(args.n_epochs + 1):
        train_ll = 0.0
        model.train()
        cnt = 0
        pos_err = .0
        ori_err = .0
        pos_ll = .0
        ori_ll = .0
        for i, (inputs, poses) in enumerate(training_set):
            x_t = inputs.to(device)
            y_t = poses.to(device)

            pos_out,ori_out,_ = model(x_t)

            loss,pos_loss,ori_loss = ps_lik_loss(y_t,pos_out,ori_out)
            # find how to compute loss in solver
            optimizer.zero_grad()
            loss.backward()
            # print(loss.item() * (x_t.size(0)))
            train_ll += loss.item() * (x_t.size(0))
            pos_ll += pos_loss * (x_t.size(0))
            ori_ll += ori_loss * (x_t.size(0))

            #torch.nn.utils.clip_grad_norm_(model.parameters(), 1.)
            optimizer.step()
        model.eval()
        val_loss = .0
        pos_err = .0
        ori_err = .0
        cnt = 0
        for i, (inputs, poses) in enumerate(test_set):
            cnt += 1
            x_t = inputs.to(device)
            y_t = poses.to(device)
            ori_true = y_t[:,3:]
            pos_true = y_t[:,:3]
            pos_out,ori_out,_ = model(x_t)
            loss = lik_loss(y_t,pos_out,ori_out)
            val_loss += loss
            pos_err += position_dist(pos_out[0].detach().cpu().numpy(),pos_true[0].cpu().numpy())
            ori_err += abs(angle_between_quat(ori_out[0].detach().cpu().numpy(),ori_true[0].cpu().numpy()))

        pos_err /= N_test
        ori_err /= N_test
        val_loss/= N_test
        train_ll /= 231
        pos_ll /= 231
        ori_ll /= 231
        print('Epoch {}: training loss = {}'.format(s,[train_ll,val_loss.item(),pos_err,ori_err,pos_ll,ori_ll]))
        writer.writerow([train_ll,val_loss.item(),pos_err,ori_err,pos_ll,ori_ll])
